<?php

namespace App\Jobs;

use Illuminate\Support\Facades\Log;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\MicrosoftAccess;
use App\Models\MicrosoftUser;
use App\Models\MicrosoftMail;
use MicrosoftOutlook;
use Carbon\Carbon;

class ProcessMicrosoft implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $access;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(MicrosoftAccess $access)
    {
        $this->access = $access;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $tokenCache = new \App\TokenStore\TokenCache;
        $tokenCache->setTokens( $this->access->access_token, $this->access->refresh_token, $this->access->token_expires  );

        $mu = MicrosoftOutlook::user();
        $user = MicrosoftUser::where('microsoft_id', $mu['id'])->first();



        $inboxFolder    = MicrosoftOutlook::folder('inbox');

        $paginatePer    = 10;
        $paginatePages  = ceil( $inboxFolder['totalItemCount'] / $paginatePer );

        //Walk over Inbox
        for($i=0; $i<$paginatePages; $i++){

            $messages = MicrosoftOutlook::messages('inbox',[ 'top' => $paginatePer, 'skip' => $i*$paginatePer ]);

            foreach($messages as $item){

//                Log::debug( print_r($item,1) );

                if ( MicrosoftMail::where('mail_id', $item['id'] )->doesntExist() ){


                    MicrosoftMail::create([
                        'mail_id'               => $item['id'],

                        'microsoft_user_id'     => (isset($user->id)) ? $user->id : null,

                        'createdDateTime'       => Carbon::parse($item['createdDateTime']),
                        'lastModifiedDateTime'  => Carbon::parse($item['lastModifiedDateTime']),
                        'receivedDateTime'      => Carbon::parse($item['receivedDateTime']),
                        'sentDateTime'          => Carbon::parse($item['sentDateTime']),
                        'subject'               => $item['subject'],
                        'parentFolderId'        => $item['parentFolderId'],
                        'conversationId'        => (isset($item['conversationId'])) ? $item['conversationId'] : '',
                        'conversationIndex'     => (isset($item['conversationIndex'])) ? $item['conversationIndex'] : '',
                        'body'                  => $item['body'],
                        'sender'                => $item['sender'],
                        'from'                  => $item['from'],
                        'toRecipients'          => $item['toRecipients'],
                        'ccRecipients'          => $item['ccRecipients'],
                        'bccRecipients'         => $item['bccRecipients'],
                        'replyTo'               => $item['replyTo'],
                    ]);
                }

            }



        }




    }
}
