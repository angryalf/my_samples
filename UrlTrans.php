<?php

namespace Vox\Services;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Route;
use Request;
use Vox\Models\CourseType;
use Vox\Models\Intensity;
use Vox\Models\Language;
use Vox\Models\LearningMode;
use Vox\Models\Location;
use Vox\Models\Courses;


class UrlTrans
{

    private $courses, $location, $language, $learning_mode, $intensity, $course_type;

    private $staticPages = [
        'about',
        'team',
        'company-language-courses',
        'jobs',
        'contact',
        'reviews',
        'prices-and-courses',
        'gtc',
        'legal',
        'unumondo',
    ];

    private $staticPages_lang = [
        'de'    => [
            'ueber-uns',
            'team',
            'firmen-sprachkurse',
            'jobs',
            'kontakt',
            'bewertungen',
            'preise-und-kurstypen',
            'agb',
            'impressum',
            'unumondo',
        ]
    ];

    protected static $instance;

    public static function getInstance()
    {
        if (is_null(static::$instance)) {
            static::$instance = new static();
        }

        return static::$instance;
    }


    public function __construct()
    {
        $this->location = Location::get();
        $this->language = Language::get();
        $this->learning_mode = LearningMode::get();
        $this->intensity = Intensity::get();
        $this->course_type = CourseType::get();
        $this->courses =  Courses::get();

    }



    public function url($full=false, $url = null, $curLang=null)
    {

        if (!$url) $url = Request::fullUrl();
        $parts = parse_url($url);
        $prefix = (app()->getLocale() == 'en') ? '' :  '_' . app()->getLocale();
        $curLang = (in_array($curLang,config('app.locales'))) ? $curLang : app()->getLocale();
        $allLang = config('app.locales');

        if ( !isset($parts['path']) ) return $url;

        $arrDimensionParams = [];
        $arrQueryParams = explode('/', $parts['path']);

        // clenup locale if it present
        foreach($allLang as $item){
            if( strpos($parts['path'], "/{$item}/" ) === false ){
                if (isset($arrQueryParams[0])) unset( $arrQueryParams[0]);
            } else {
                if (isset($arrQueryParams[0])) unset( $arrQueryParams[0]);
                if (isset($arrQueryParams[1])) unset( $arrQueryParams[1]);
            }
        }
        $arrQueryParams = array_merge( array(), $arrQueryParams );

        // make translation for static pages
        if ( isset($arrQueryParams[0]) && count($arrQueryParams) == 1 ){

            if ( in_array( $arrQueryParams[0], $this->staticPages ) ){
                //requested page on EN translation
                $sk = array_search( $arrQueryParams[0], $this->staticPages );

            } else {
                $sk = false;
                // search on another lang
                foreach ($allLang as $lang){
                    if ( key_exists($lang, $this->staticPages_lang) && in_array( $arrQueryParams[0], $this->staticPages_lang[$lang] ) ){
                        $sk = array_search( $arrQueryParams[0], $this->staticPages_lang[$lang] );
                    }
                }

            }


            if ( $sk === false ){
                // translation do not exist in static pages
            } else {
                // get right translation for static page
                if ( $curLang == 'en'){
                    return route( "{$curLang}.". str_replace("-","_", $this->staticPages[$sk]) );
                    return"";

                } else {
                    return url( "{$curLang}/". $this->staticPages_lang[$curLang][$sk] );
                }
            }

        }

        $arrDimensionParams = [];
        foreach ($arrQueryParams as $item){

            $filtered = $this->location->filter(function ($value, $key) use ($allLang, $item ) {
                foreach ($allLang as $lang){
                    if ($lang == 'en'){
                        if ($item === $value->url_key) return true;
                    } else {
                        if ($item === $value->{"url_key_{$lang}"} ) return true;
                    }
                }
                return false;
            })->first();
            if ( $filtered ) $arrDimensionParams['location'] = $filtered;

            $filtered = $this->language->filter(function ($value, $key) use ($allLang, $item ) {
                foreach ($allLang as $lang){
                    if ($lang == 'en'){
                        if ($item === $value->url_key) return true;
                    } else {
                        if ($item === $value->{"url_key_{$lang}"} ) return true;
                    }
                }
                return false;
            })->first();
            if ( $filtered ) $arrDimensionParams['language'] = $filtered;

            $filtered = $this->learning_mode->filter(function ($value, $key) use ($allLang, $item ) {
                foreach ($allLang as $lang){
                    if ($lang == 'en'){
                        if ($item === $value->url_key) return true;
                    } else {
                        if ($item === $value->{"url_key_{$lang}"} ) return true;
                    }
                }
                return false;
            })->first();
            if ( $filtered ) $arrDimensionParams['learning_mode'] = $filtered;

            $filtered = $this->intensity->filter(function ($value, $key) use ($allLang, $item ) {
                foreach ($allLang as $lang){
                    if ($lang == 'en'){
                        if ($item === $value->url_key) return true;
                    } else {
                        if ($item === $value->{"url_key_{$lang}"} ) return true;
                    }
                }
                return false;
            })->first();
            if ( $filtered ) $arrDimensionParams['intensity'] = $filtered;

            $filtered = $this->course_type->filter(function ($value, $key) use ($allLang, $item ) {
                foreach ($allLang as $lang){
                    if ($lang == 'en'){
                        if ($item === $value->url_key) return true;
                    } else {
                        if ($item === $value->{"url_key_{$lang}"} ) return true;
                    }
                }
                return false;
            })->first();
            if ( $filtered ) $arrDimensionParams['course_type'] = $filtered;


        }



        $parts['path'] = "/{$curLang}";

        if (isset($arrDimensionParams['location'])){
            if ($curLang == 'en'){
                $parts['path'] .= "/" . $arrDimensionParams['location']->url_key;
            } else {
                $parts['path'] .= "/" . $arrDimensionParams['location']->{"url_key_{$curLang}"};
            }
        } else {
            $parts['path'] .= ($full || isset($arrDimensionParams['language']) || isset($arrDimensionParams['learning_mode']) || isset($arrDimensionParams['intensity']) || isset($arrDimensionParams['course_type']) ) ? "/-" : '';
        }

        if (isset($arrDimensionParams['language'])){
            if ($curLang == 'en'){
                $parts['path'] .= "/" . $arrDimensionParams['language']->url_key;
            } else {
                $parts['path'] .= "/" . $arrDimensionParams['language']->{"url_key_{$curLang}"};
            }
        } else {
            $parts['path'] .= ($full || isset($arrDimensionParams['learning_mode']) || isset($arrDimensionParams['intensity']) || isset($arrDimensionParams['course_type']) ) ? "/-" : '';
        }

        if (isset($arrDimensionParams['learning_mode'])){
            if ($curLang == 'en'){
                $parts['path'] .= "/" . $arrDimensionParams['learning_mode']->url_key;
            } else {
                $parts['path'] .= "/" . $arrDimensionParams['learning_mode']->{"url_key_{$curLang}"};
            }
        } else {
            $parts['path'] .= ($full || isset($arrDimensionParams['intensity']) || isset($arrDimensionParams['course_type']) ) ? "/-" : '';
        }

        if (isset($arrDimensionParams['intensity'])){
            if ($curLang == 'en'){
                $parts['path'] .= "/" . $arrDimensionParams['intensity']->url_key;
            } else {
                $parts['path'] .= "/" . $arrDimensionParams['intensity']->{"url_key_{$curLang}"};
            }
        } else {
            $parts['path'] .= ($full || isset($arrDimensionParams['course_type'])) ? "/-" : '';
        }

        if (isset($arrDimensionParams['course_type'])){
            if ($curLang == 'en'){
                $parts['path'] .= "/" . $arrDimensionParams['course_type']->url_key;
            } else {
                $parts['path'] .= "/" . $arrDimensionParams['course_type']->{"url_key_{$curLang}"};
            }
        } else {
            $parts['path'] .= ($full) ? "/-" : '';
        }


        return $this->unparse_url( $parts );

    }


    private function unparse_url($parsed_url) {
        $scheme   = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : '';
        $host     = isset($parsed_url['host']) ? $parsed_url['host'] : '';
        $port     = isset($parsed_url['port']) ? ':' . $parsed_url['port'] : '';
        $user     = isset($parsed_url['user']) ? $parsed_url['user'] : '';
        $pass     = isset($parsed_url['pass']) ? ':' . $parsed_url['pass']  : '';
        $pass     = ($user || $pass) ? "$pass@" : '';
        $path     = isset($parsed_url['path']) ? $parsed_url['path'] : '';
        $query    = isset($parsed_url['query']) ? '?' . $parsed_url['query'] : '';
        $fragment = isset($parsed_url['fragment']) ? '#' . $parsed_url['fragment'] : '';
        return "$scheme$user$pass$host$port$path$query$fragment";
    }


}