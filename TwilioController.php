<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\TwilioHelper;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Models\Twilio;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class TwilioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::user()->hasPermissionTo("admin view twilio")) {
            return redirect()->route('admin.');
        }

        $twilio = Twilio::orderBy('created_at', 'desc')->paginate(10);


        return view('admin.twilio.index', compact('twilio') );

    }


    public function message(Request $request)
    {
        if (!Auth::user()->hasPermissionTo("admin send twilio")) {
            return redirect()->route('admin.');
        }

        $users = User::orderBy('name', 'asc')->get();
        $types = ['SMS','Call'];

        if ( $request->isMethod('POST') ){

            $validatedData = $request->validate([
                'users' => 'required',
                'text' => 'required|max:160',
                'type'  => 'required'
            ]);

            foreach ($request->get('users') as $uid){

                $user = $users->where('id', $uid)->first();
                $message = $this->insertSnippets( $request->get('text'), $user );

                $this->sendMessage( $request->get('type'), optional($user->profile)->phone, $message );

            }

            return redirect( route('admin.twilio.index') )->with('success', 'Message send.');

        }

        $users = $users->pluck('name','id');
        return view('admin.twilio.message', compact('users','types'));
    }



    private function sendMessage($type = null, $phone=null, $message=null)
    {
        /**
         * Do not send any if number or message empty
         */
        if ( $phone == null || $message == null ) return false;

        switch ($type){

            case 1:
                TwilioHelper::sendVoiceSMS($phone, $this->cleanMessage($message));
                break;

            case 0:
                TwilioHelper::sendSMS($phone, $this->cleanMessage($message));
                break;

        }



    }

    private function cleanMessage($msg=null)
    {
        $msg = strip_tags( trim($msg) );
        return $msg;
    }

    private function insertSnippets($msg=null, $user=null)
    {

        $tags = [ "{userEmail}" ];

        $vals = [ $user->email ];


        return str_replace($tags, $vals, $msg);

    }


//    /**
//     * Show the form for creating a new resource.
//     *
//     * @return \Illuminate\Http\Response
//     */
//    public function create()
//    {
//        //
//    }
//
//    /**
//     * Store a newly created resource in storage.
//     *
//     * @param  \Illuminate\Http\Request  $request
//     * @return \Illuminate\Http\Response
//     */
//    public function store(Request $request)
//    {
//        //
//    }
//
//    /**
//     * Display the specified resource.
//     *
//     * @param  \App\Models\Twilio  $twilio
//     * @return \Illuminate\Http\Response
//     */
//    public function show(Twilio $twilio)
//    {
//        //
//    }
//
//    /**
//     * Show the form for editing the specified resource.
//     *
//     * @param  \App\Models\Twilio  $twilio
//     * @return \Illuminate\Http\Response
//     */
//    public function edit(Twilio $twilio)
//    {
//        //
//    }
//
//    /**
//     * Update the specified resource in storage.
//     *
//     * @param  \Illuminate\Http\Request  $request
//     * @param  \App\Models\Twilio  $twilio
//     * @return \Illuminate\Http\Response
//     */
//    public function update(Request $request, Twilio $twilio)
//    {
//        //
//    }
//
//    /**
//     * Remove the specified resource from storage.
//     *
//     * @param  \App\Models\Twilio  $twilio
//     * @return \Illuminate\Http\Response
//     */
//    public function destroy(Twilio $twilio)
//    {
//        //
//    }
}
