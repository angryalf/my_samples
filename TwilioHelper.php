<?php

namespace App\Helpers;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use App\Models\Setting;
use Twilio\Exceptions\RestException;
use Twilio\Rest\Client;
use Twilio\TwiML\VoiceResponse;
use App\Models\Twilio;


class TwilioHelper {



    public static function sendSMS($number=null, $msg=null)
    {

        $tw = Twilio::create([
            'from'      => Setting::env("TWILIO_FROM"),
            'to'        => $number,
            'msg'       => $msg,
            'user_id'   => (Auth::guest()) ? null : Auth::user()->id,
            'delayed'   => 0,
        ]);

        try{

            $client = new Client( Setting::env("TWILIO_SID"), Setting::env("TWILIO_TOKEN"));

            // Use the client to do fun stuff like send text messages!
            $result = $client->messages->create(
            // the number you'd like to send the message to
                $number,
                array(
                    // A Twilio phone number you purchased at twilio.com/console
                    'from' => Setting::env("TWILIO_FROM"),
                    // the body of the text message you'd like to send
                    'body' => $msg
                )
            );

            $tw->sid = $result->sid;
            $tw->save();

        } catch (RestException $e){

            return false;

        }



        return $result;
    }


    public static function sendVoiceSMS($number=null, $msg=null)
    {
        $tw = Twilio::create([
            'from'      => Setting::env("TWILIO_FROM"),
            'to'        => $number,
            'msg'       => $msg,
            'user_id'   => (Auth::guest()) ? null : Auth::user()->id,
            'delayed'   => 0,
        ]);

        $fileName = md5($tw->id) . ".xml";

        $response = new VoiceResponse();
        $response->say(
            $msg,
            [
                'voice' => 'Polly.Ruben',
                'rate'  => 'slow'
            ]
        );
//        echo $response;
        Storage::disk('public')->put( $fileName, $response );



        try{


            $client = new Client( Setting::env("TWILIO_SID"), Setting::env("TWILIO_TOKEN"));
            $result = $client->account->calls->create(
//            $to_number,
                $number,
                Setting::env("TWILIO_FROM"),
                array(
                    "url" => Storage::disk('public')->url($fileName)
                )
            );

            $tw->sid = $result->sid;
            $tw->save();


        } catch (RestException $e){

            return false;

        }


        return $result;

    }



}
