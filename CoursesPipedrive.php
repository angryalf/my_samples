<?php

namespace Vox\Services;

use Carbon\CarbonPeriod;
use Devio\Pipedrive\Pipedrive;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Route;
use Carbon\Carbon;
use Request;

use Vox\Models\Courses;
use Vox\Models\CourseType;
use Vox\Models\PriceRule;
use Vox\Models\Intensity;
use Vox\Models\Language;
use Vox\Models\LearningMode;
use Vox\Models\Location;

use Vox\Models\PipedriveProduct;
use Vox\Services\DecoratedPipedrive;
use Vox\Models\PipedriveDealField;
use Vox\Models\PipedriveField;
use Vox\Models\PipedriveProductField;



class CoursesPipedrive
{
    private $pipedrive;
    private $languagesCache;


    public function __construct(DecoratedPipedrive $decoratedPipedrive)
    {
        $this->pipedrive = $decoratedPipedrive;

    }


    public function getDeal($id=null)
    {
        return $this->pipedrive->getDecoree()->deals()->find($id)->getData();
    }

    public function calculateFinalPrice(Courses $courses)
    {
        $currentDate = Carbon::now();
        $priceRuleQuery = PriceRule::query();
        $priceRuleQuery->where('start', '<=', $currentDate)->where('end', '>=', $currentDate);

        $dimensionColumns = [
            'dimension_location_code',
            'dimension_language_code',
            'dimension_learning_mode_code',
            'dimension_course_type_code',
            'dimension_intensity_code'
        ];

        foreach($dimensionColumns as $col) {
            $priceRuleQuery->whereIn($col, ['*', $courses->{$col}]);

        }

        // Take most specific price rule if there are more than 1
        /*
        $priceRule = $priceRuleQuery->first();
        $minStars = count($dimensionColumns);
        foreach ($priceRuleQuery->get() as $pr) {
            $countStars = 0;
            foreach($dimensionColumns as $col) {
                if ($pr->{$col} == '*') {
                    $countStars++;
                }
            }
            if ($minStars >= $countStars) {
                $minStars = $countStars;
                $priceRule = $pr;
            }
        }
        */


//        Log::debug( print_r($priceRule,1) );


        // Take the the biggest discount
        $discount = 0;
        /** @var PriceRule $pr */
        foreach ($priceRuleQuery->get() as $pr) {
            $calculatedDiscount = $pr->calculateDiscount($courses->price);
            if ($calculatedDiscount > $discount) {
                $discount = $calculatedDiscount;
            }
        }
        $result = $courses->price - $discount >= 0 ? $courses->price - $discount : $courses->price;
        return $result;
    }

    public function addDeal($data=array(), $pipelineStageId=null)
    {

        $dealFields = PipedriveDealField::get();

        $productFields = PipedriveProductField::get();
        $productLocations = collect( json_decode($productFields->where('key',config('services.pipedrive.fields.product.location'))->first()->options_json) );
        $productLanguages = collect( json_decode($productFields->where('key',config('services.pipedrive.fields.product.language'))->first()->options_json) );
        $productLearningMode = collect( json_decode($productFields->where('key',config('services.pipedrive.fields.product.learning_mode'))->first()->options_json) );


        $courserIds = collect( json_decode($dealFields->where('key', config('services.pipedrive.fields.deal.courses'))->first()->options_json) );
        $intensityId = collect( json_decode($dealFields->where('key', config('services.pipedrive.fields.deal.intensity'))->first()->options_json) );

        $personData = array();
        $persons = $this->pipedrive->getDecoree()->persons->findByName($data['email'], ['search_by_email' => 1])->getData();
        if ($persons){

            $personData['name'] = $persons[0]->name;
            $personData['email'] = $persons[0]->email;
            $personData['phone'] = $persons[0]->phone;
            $personData['id']   = $persons[0]->id;

        } else {
            $personData['name'] = $data['name'];
            $personData['email'] = $data['email'];
            $personData['phone'] = $data['phone'];

            $persons = $this->pipedrive->getDecoree()->persons()->add($personData)->getData();
            $personData['id']   = $persons->id;

        }


        $voxLanguage = ($data['dimension_language_code']) ? $data['dimension_language_code'] : '';
        $voxLocation = ($data['dimension_location_code']) ? $data['dimension_location_code'] : '';
        $voxCourse = ($data['dimension_course_type_code']) ? $data['dimension_course_type_code'] : '';
        $voxIntensity = ($data['dimension_intensity_code']) ? config('services.pipedrive.mapping.dimension_intensity')[$data['dimension_intensity_code']]  : '';
        $voxLearningMode = ($data['dimension_learning_mode_code']) ? $data['dimension_learning_mode_code'] : '';


        //Lets create a deal
        $dealData = array();
        $dealData['title'] =$personData['name'] . ' ' . $voxCourse . ' ' . $voxIntensity;
        $dealData['expected_close_date'] = date("Y-m-d", time() + 14 * 24 * 60 * 60);
        $dealData['person_id'] = $personData['id'];

        //We do not have date yet, only in preferences step user will fill this date
        $dealData[ config('services.pipedrive.fields.deal.start_date') ] = $this->getDefaultStartDate();

        $dealData[ config('services.pipedrive.fields.deal.courses') ] = optional($courserIds->where('label', $voxCourse)->first())->id;
        $dealData[ config('services.pipedrive.fields.deal.intensity') ] = optional($intensityId->where('label', $voxIntensity)->first())->id;

        //TODO: Default status info
        $dealData[ config('services.pipedrive.fields.deal.status_info') ] = config('services.pipedrive.defaults.deal.status_info');


        $dealData['stage_id']   = $pipelineStageId;


        try{

            $pipedriveDeal = $this->pipedrive->getDecoree()->deals()->add( $dealData );

            return $pipedriveDeal->getData()->id;


        } catch (\Exception $e){

            Log::debug( 'Cant create pipeline deal');
            Log::debug( $e->getMessage() );
            return false;

        }

    }

    public function attachProductsToDeal($productData)
    {

        $result = $this->pipedrive->getDecoree()->deals()->addProduct($productData['id'],$productData['product_id'],$productData['item_price'],$productData['quantity'] );


        return $result->getData();
    }

    public function getDefaultStartDate()
    {
        $startDate = date('Y-m-d',(date('d')<15?$startDate=strtotime('+3 Weekdays'):strtotime('last day of this month +1 Weekdays')));
        return $startDate;
    }

    public function collectCoursesWithoutTeacher()
    {
        $coursesWithoutTeacher = [];

        $_pipedriveProductFields = PipedriveProductField::all();
        $_pipedriveDealFields = PipedriveDealField::all();

        $deals = $this->pipedrive->getAllDeals(config('services.pipedrive.filter.courses_without_teachers'));

        foreach ($deals as $deal) {
            if ($deal->person_name != '' && $deal->person_name != 'Unknown Teacher') {
                // This should never be the case, as the data must come already correctly from the filter
                continue;
            }

            //TODO need minimize requests. Now one request for each deal. Need to wait API update
            $pipedriveRequestData = $this->pipedrive->getDecoree()->deals->products($deal->id, ['include_product_data' => 1])->getData();

            // If no product attached to deal, because we could not exctract needed information
            if ($pipedriveRequestData == null) {
                continue;
            }

            $pipedriveProduct = $pipedriveRequestData[0]->product;
            $productOptions = $this->getOptionsArray($_pipedriveProductFields, $pipedriveProduct);
            $dealOptions = $this->getOptionsArray($_pipedriveDealFields, $deal);

            $voxLocation = 'External (not in school)';
            if (in_array($deal->pipeline_id, [config('services.pipedrive.pipeline.CH01-acquisition.id'), config('services.pipedrive.pipeline.CH01-courses.id')])) {
                $voxLocation = 'Winterthur';
            } if (in_array($deal->pipeline_id, [config('services.pipedrive.pipeline.CH02-acquisition.id'), config('services.pipedrive.pipeline.CH02-courses.id')])) {
                $voxLocation = 'Zurich';
            }

            $rate = '';
            if (stristr($voxLocation, 'External') !== false) {
                $rate = '40.-';
            } else {
                if ($deal->participants_count > 0 && $deal->participants_count <= 2){
                    $rate = '50.-';
                } else if ($deal->participants_count == 3){
                    $rate = '60.-';
                } else if ($deal->participants_count == 4) {
                    $rate = '70.-';
                } else if ($deal->participants_count >= 5) {
                    $rate = '80.-';
                }
            }




            array_push($coursesWithoutTeacher, array(
                // TODO: this should somehow be derived by name from the database
                "id" => (isset($dealOptions['Intensity']) && $dealOptions['Intensity'] == 'Flexible' ? 'P'.$deal->id :'G'.$deal->id),
                "language" => $this->resolveLanguageNameByCode($productOptions['Language']),
                "course" => (isset($dealOptions['Course']) ? $dealOptions['Course'] : ''),
                "course_start_date" => ($deal->{'66105d05557b017bfc48b516f19d0eb4c3b3c49d'} ?: ''),
                "schedule" => (isset($dealOptions['Availabilities']) ? $this->condenseAvailabilities( $dealOptions['Availabilities'] ): []),
                "intensity" => (isset($dealOptions['Intensity']) ? $dealOptions['Intensity'] : ''),
                "location" => $voxLocation,
                "rate" => $rate,
                "participants" => $deal->participants_count

            ));

        }

        $sort = array();

        foreach($coursesWithoutTeacher as $k => $v)
        {
            $sort['location'][$k] = $v['location'];
            $sort['language'][$k] = $v['language'];
            $sort['course_start_date'][$k] = $v['course_start_date'];
        }
        array_multisort($sort['location'], SORT_DESC, $sort['course_start_date'], SORT_ASC, $sort['language'], SORT_ASC, $coursesWithoutTeacher);

        return  $coursesWithoutTeacher ;
    }

    private function resolveLanguageNameByCode($code) {
        if (!isset($this->languagesCache)) {
            $this->languagesCache = Language::all();
        }
        foreach($this->languagesCache as $languageObj) {
            if ($languageObj->code == $code) {
                return $languageObj->name;
            }
        }
    }

    private function getOptionsArray($fieldsArray /*all deal fields*/, $pipedriveObject /* current deal*/)
    {
        $returnArray = [];

        foreach ($fieldsArray as $field)
        {
            if (($field->field_type=="enum" || $field->field_type=="set") &&  isset($pipedriveObject->{$field->key}))
            {
                $field->options = ($field->options_json) ? json_decode($field->options_json) : array();
                //can be few options separated by comma example 188,189
                $pieces = explode(',', $pipedriveObject->{$field->key});
                $labels = [];

                foreach ($pieces as $piece)
                {
                    foreach ($field->options as $option)
                    {
                        if ($option->id == $piece)
                        {
                            array_push($labels,$option->label);
                            break;
                        }
                    }
                }

                $returnArray[$field->key] = implode(',',$labels);
                $returnArray[$field->name] = implode(',',$labels);

            }
        }

        return $returnArray;
    }

    private function condenseAvailabilities($availabilitiesStr = null)
    {

        $segments = explode(',', $availabilitiesStr);
        $period = array();

        foreach ($segments as $item){

            $ti = substr($item, -13);
            $di = trim( substr($item, 0, strlen($item) - strlen($ti)) );

            $inval = explode(' - ', $ti);

            $dateStart = Carbon::now()->setTime( explode(':',$inval[0])[0], explode(':',$inval[0])[1], 0 );
            $dateEnd   = Carbon::now()->setTime( explode(':',$inval[1])[0], explode(':',$inval[1])[1], 0 );

            $period[$di][] = ['start' => $dateStart, 'end' => $dateEnd];
        }

        $merged = array();
        foreach($period as $day=>$array){

            $result=array();
            usort($array,function($a,$b){ return $a['start']<=>$b['end']; });  // order by start_date ASC
            if (!isset($x)) $x = -1;
            foreach($array as $i=>$row){
                if($i && $row['start']<=date('Y-m-d',strtotime("{$result[$x]['end']} +1 day"))){  // not the first iteration and dates are within current group's range
                    if($row['end']>$result[$x]['end']){  // only if current end_date is greater than existing end_date
                        $result[$x]['end']=$row['end'];  // overwrite end_date with new end_date in group
                    }
                    $result[$x]['merged_ids'][]=$i;  // append id to merged_ids subarray
                }else{  // first iteration or out of range; start new group
                    if($i){  // if not first iteration
                        $result[$x]['merged_ids']=implode(', ',$result[$x]['merged_ids']);  // convert previous group's id elements to csv string
                    }else{  // first iteration
                        $x=-1;  // declare $x as -1 so that it becomes 0 when incremented with ++$x
                    }
                    $result[++$x]=['merged_ids'=>[$i],'start'=>$row['start'],'end'=>$row['end']]; // declare new group
                }
            }
            $result[$x]['merged_ids']=implode(', ',$result[$x]['merged_ids']);  // convert final merged_ids subarray to csv string

            $merged[$day] = $result;
        }


        // Sort by weekdays
        $week_order = array_flip( $this->getDays());
        uksort($merged,
            function($a, $b) use ($week_order) { return $week_order[$a] - $week_order[$b]; });


        $condensed = array();
        foreach ($merged as $day=>$times){

            foreach ($times as $periods){
                $condensed[] = "{$day} ".$periods['start']->format('H:i') .' - '.$periods['end']->format('H:i');

            }



        }

        return $condensed;
    }


    private function getDays($short=true, $fromSunday=false)
    {
        $timestamp = ($fromSunday) ? strtotime('next Sunday') : strtotime('next Monday');
        $days = array();
        for ($i = 0; $i < 7; $i++) {
            $days[] = date( ($short) ? 'D' :'l', $timestamp);
            $timestamp = strtotime('+1 day', $timestamp);
        }
        return $days;
    }


}